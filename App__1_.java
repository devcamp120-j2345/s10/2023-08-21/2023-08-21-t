import com.devcamp.task54.*;

public class App {
    public static void main(String[] args) throws Exception {
        Author author1 = new Author("Trần Tuấn Hùng","hungtv@devcamp.edu.vn",'m');
        Author author2 = new Author("Nguyễn Văn Vinh","vinhnv@devcamp.edu.vn",'m');
        
        //System.out.println("Hello, World1! "+author1.toString());
        //System.out.println("Hello, World1! "+author2);

        Book book1 = new Book("Trăm năm cô đơn",author1,3.0,100);
        Book book2 = new Book("Đồi gió hú",author2,5.0);
        System.out.println("Thông tin cuốn sách 1:" + book1);
        System.out.println("Thông tin cuốn sách 2:" + book2);
        
    }
}
